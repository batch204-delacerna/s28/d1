//CRUD Operations

// Insert documents (CREATE)
/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})
		
		Insert Many Document
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});



db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stepehnhawking@mail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"department": "none"
	},
]);


//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: false
	*/

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to JavaScript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": true
	}
]);


// Find Documents (Read)
/*
	Syntax:
		db.collectionName.find() - retrieves all of the documents
		
		db.collectionName.find({"criteria": "value"}) - retrieves all our documents that will match with our criteria
		
		db.collectionName.findOne({}) will return the first document in our collection
		db.collectionName.findOne({"criteria": "value"}) - will return the first document in our collection that will match our criteria

*/

db.users.find()

db.users.find({
	"firstName": "Jane"
})

db.users.findOne();

db.users.findOne({
	"department": "none"
});


// Updating Documents (Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated"": "value"
			}
		})
*/

db.users.insertOne({
	"firstName": "TEST",
	"lastName": "TEST",
	"age": 69,
	"email": "TEST@mail.com",
	"department": "none"
});


db.users.updateOne({
		"firstName": "TEST"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)


// Updating Multiple Documents
db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}
);

db.users.updateOne(
	{
		"firstName": "Jane",
		"lastName": "Doe"
	},
	{
		$set: {
			"department": "Operations"
		}
	}
)


//removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}
)

db.users.updateMany(
	{},
	{
		$rename: {
			"department": "dept"
		}
	}
)


/*
	Mini-Activity:
		Mini Activity: 
		1. In our courses collection, update the HTML 101 course
			- Make the isActive to false
		2. Add enrollees field to all the documents in our courses collection
			-Enrollees: 10

*/

db.courses.updateMany(
	{
            "name": "HTML 101"
    },
	{
    	$set: {
			"isActive": false
        }
	},
);

db.courses.updateMany(
    {},
	{
    	$set: {
			"enrollees": 10
        }
	}
);

db.courses.find();

// Delete documents
/*
	Syntax:
		-db
*/

db.users.insertOne({
	"firstName":"Test"
});

db.users.deleteOne({"firstName": "Test"});

db.users.deleteOne({"dept": "HR"});

db.courses.deleteMany({});